# Directus SQLite3 Reference bug


## How to run
1. fill .env file
2. docker compose up --build

## Reproduce Bug
1. Start app 
2. add page with data and 2 buttons
3. switch in Dockerfile to use schema with removed image field
4. docker compose up --build

-> Buttons reference is set to null

