FROM directus/directus:9.23.1

WORKDIR /directus

# COPY ./schema-removed-field.yml ./schema-removed-field.yml
COPY ./schema.yml ./schema.yml

# CMD npx directus bootstrap && npx directus schema apply --yes ./schema-removed-field.yml && npx directus start
CMD npx directus bootstrap && npx directus schema apply --yes ./schema.yml && npx directus start